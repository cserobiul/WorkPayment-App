# Work Payment Application 

Work Payment Application is a web application. I believe development must be an enjoyable, creative experience to be truly fulfilling.
Work Payment App is accessible, yet powerful and robust applications. 

## Official Documentation

Documentation for this application can be found on the [My website](#).


## Security Vulnerabilities

If you discover a security vulnerability within this application, please send an e-mail to Taylor Otwell at amrobi15@yahoo.com. All security vulnerabilities will be promptly addressed.

## License & Copyright 

The Work Payment App isn't open-sourced  application software that licensed under the [MIT license](#).